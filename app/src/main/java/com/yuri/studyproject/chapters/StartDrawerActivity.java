package com.yuri.studyproject.chapters;

import android.view.View;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseDrawerActivity;

public class StartDrawerActivity extends BaseDrawerActivity {
    @Override
    public int setLayoutRes() {
        return R.layout.content_main;
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

}

