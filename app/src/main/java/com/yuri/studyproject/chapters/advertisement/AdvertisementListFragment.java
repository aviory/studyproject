package com.yuri.studyproject.chapters.advertisement;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseActivity;
import com.yuri.studyproject.chapters.base.BaseFragment;
import com.yuri.studyproject.datastorage.provider.TableAdvertisOptions;
import com.yuri.studyproject.ui.adapters.AdvertiseAdapter;

import butterknife.Bind;

/**
 * Created by Jack_killer on 24.10.2015.
 */
public class AdvertisementListFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final static String TAG = AdvertisementListFragment.class.getSimpleName();
    private static final String PARENT_CATEGORY_INDEX = "parent_index";
    private AdvertiseAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Bind(R.id.ad_recycler_view)
    protected RecyclerView recyclerView;

    @Override
    protected int setLayoutRes() {
        return R.layout.list_ads;
    }

    @Override
    protected void afterCreateView() {
        Bundle args = getArguments();
        if (args == null) return;
        int parentAdvertisIndex = args.getInt(PARENT_CATEGORY_INDEX, -1);
        Log.d(TAG, "Index of parent: " + parentAdvertisIndex);


        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        Cursor cursor = getContext().getContentResolver().query(TableAdvertisOptions.CONTENT_URI, null, null, null, null);
        mAdapter = new AdvertiseAdapter(getContext(), cursor);
        recyclerView.setAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
    }


    public static void launch(BaseActivity advertisementActivity, int parentIndex) {
        Bundle bundle = new Bundle();
        bundle.putInt(PARENT_CATEGORY_INDEX, parentIndex);
        AdvertisementListFragment advertisementFragment = new AdvertisementListFragment();
        advertisementFragment.setArguments(bundle);
        advertisementActivity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_right,
                        R.anim.pull_in_left, R.anim.push_out_left)
                .add(R.id.container, advertisementFragment)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.CursorLoader(
                getContext(), TableAdvertisOptions.CONTENT_URI, null, null, null, null
        );
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        mAdapter.swapCursor(null);
    }
}
