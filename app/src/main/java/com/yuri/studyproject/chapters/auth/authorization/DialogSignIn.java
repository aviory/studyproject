package com.yuri.studyproject.chapters.auth.authorization;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yuri.studyproject.R;

/**
 * Created by jack on 15.10.15.
 */
public class DialogSignIn extends DialogFragment implements View.OnClickListener {
    private EditText etLogin;
    private EditText etPassword;
    private TextView tvRegistration;
    private Button btnAction;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_signin, container);
        initAllView(rootView);
        initListener();
        return rootView;

    }

    private void initListener() {
        btnAction.setOnClickListener(this);
        tvRegistration.setOnClickListener(this);
    }

    private void initAllView(View v) {
        etLogin = (EditText) v.findViewById(R.id.edit_text_login);
        etPassword = (EditText) v.findViewById(R.id.edit_text_password);
        btnAction = (Button) v.findViewById(R.id.btn_action_sign_in);
        tvRegistration = (TextView) v.findViewById(R.id.text_view_registration);
    }

    @Override
    public void onClick(View v) {
        //TODO Использовать интерфейс для предачи данных на сервер (может быть в потоке с прогресбаром)
        switch (v.getId()) {
            case R.id.btn_action_sign_in:
                //временное сообщение
                Snackbar.make(getActivity().getCurrentFocus(), "Страница \"Авторизации\" пока не создана", Snackbar.LENGTH_LONG).show();
                break;
            case R.id.text_view_registration:
                //временное сообщение
                Snackbar.make(getActivity().getCurrentFocus(), "Страница \"Регистрации\" пока не создана", Snackbar.LENGTH_LONG).show();
                break;
        }
        dismiss();
    }
}
