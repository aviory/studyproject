package com.yuri.studyproject.chapters.base.BaseInterface;

/**
 * Created by Юрий on 24.10.2015.
 */
public abstract class BaseApiOperation {

    Object object;

    public BaseApiOperation(Object request) {
        this.object = object;
    }

    public abstract void init();

    public abstract Object onResult(Object request);

    public abstract Exception onError();

}