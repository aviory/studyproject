package com.yuri.studyproject.chapters.base.BaseInterface;

import com.yuri.studyproject.domain.Ad;
import com.yuri.studyproject.domain.Country;
import com.yuri.studyproject.domain.Currency;
import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

/**
 * Created by Юрий on 24.10.2015.
 */
public interface BaseDataInterface {

    //From AppInit
    public List<Category> getAllCategoties();

    public List<Country> getAllCountry();

    public List<Currency> getAllCurrency();

    //From CategoryActivity
    public List<Category> getCategoriesFromParentId(long id);

    //From MessageActivity
    public List<Ad> getAdFromParentCategory(long id);

}
