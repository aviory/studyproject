package com.yuri.studyproject.chapters.base.BaseInterface;

import com.yuri.studyproject.domain.Country;
import com.yuri.studyproject.domain.Currency;
import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

/**
 * Created by Юрий on 24.10.2015.
 */
public interface BaseDataSaveInterface {
    public void storeAllCategories(List<Category> categories);
    public void storeAllCountry(List<Country> countries);
    public void storeAllCurrency(List<Currency> currencies);
}
