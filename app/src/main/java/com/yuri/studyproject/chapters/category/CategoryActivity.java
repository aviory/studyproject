package com.yuri.studyproject.chapters.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseActivity;
import com.yuri.studyproject.network.NetApiInterface;
import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

/**
 * Created by Юрий on 14.10.2015.
 */
public class CategoryActivity extends BaseActivity {
    private final static String TAG = CategoryActivity.class.getSimpleName();
    public final static String PARENT_CATEGORY_INDEX = "parent_index";
    private NetApiInterface netApiInterface;
    private List<Category> categories;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;



    public static void launch(Context context, int parentCategoryIndex){
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.putExtra(PARENT_CATEGORY_INDEX, parentCategoryIndex);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        Intent args = getIntent();
        if (args==null) return;

        int parentIndex = args.getIntExtra(PARENT_CATEGORY_INDEX, -1);
        CategoryFragment.launch(this, parentIndex);
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_category;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "OnBackPressed!");
        int size = getSupportFragmentManager().getBackStackEntryCount();
        Log.d(TAG, "Fragments size: "+size);
        if (size==0) finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
