package com.yuri.studyproject.chapters.category;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseActivity;
import com.yuri.studyproject.chapters.base.BaseFragment;
import com.yuri.studyproject.network.NetApiInterface;
import com.yuri.studyproject.network.mockapi.MockApiClient;
import com.yuri.studyproject.network.pojo.Category;
import com.yuri.studyproject.ui.adapters.CategoryAdapter;

import java.util.List;

import butterknife.Bind;

/**
 * Created by Юрий on 14.10.2015.
 */
public class CategoryFragment extends BaseFragment {
    private final static String TAG = CategoryFragment.class.getSimpleName();
    private final static String PARENT_CATEGORY_INDEX = "parent_index";
    private NetApiInterface netApiInterface;
    private List<Category> categories;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Bind(R.id.my_recycler_view)
    protected RecyclerView recyclerView;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_category;
    }

    @Override
    protected void afterCreateView() {
        Bundle args = getArguments();
        if (args == null) return;
        int parentCategotyIndex = args.getInt(PARENT_CATEGORY_INDEX, -1);
        Log.d(TAG, "Index of parent: " + parentCategotyIndex);


        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        netApiInterface = new MockApiClient();
        categories = netApiInterface.getCategories(parentCategotyIndex);

        String[] str = new String[categories.size()];
        for (int i = 0; i < str.length; i++) {
            str[i] = categories.get(i).getItemName();
        }

        mAdapter = new CategoryAdapter((BaseActivity) getActivity(), str);
        recyclerView.setAdapter(mAdapter);
    }

    public static void launch(BaseActivity activity, int parentIndex) {
        Bundle bundle = new Bundle();
        bundle.putInt(PARENT_CATEGORY_INDEX, parentIndex);
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bundle);
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_right,
                        R.anim.pull_in_left, R.anim.push_out_left)
                .add(R.id.container, categoryFragment)
                .addToBackStack(null)
                .commit();
    }

}
