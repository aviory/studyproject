package com.yuri.studyproject.chapters.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.TextView;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseDrawerActivity;
import com.yuri.studyproject.network.NetApiInterface;
import com.yuri.studyproject.network.mockapi.MockApiClient;
import com.yuri.studyproject.network.pojo.BaseCategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class AboardMainActivity extends BaseDrawerActivity{
    private final static String TAG = AboardMainActivity.class.getSimpleName();
    private List<BaseCategory> baseCategories;
    private NetApiInterface netApiInterface;
    private PagerAdapter mPagerAdapter;

    @Bind(R.id.pager)
    protected ViewPager mPager;
    @Bind(R.id.tvNumberPages)
    protected TextView tvNumberPages;

    @Override
    public int setLayoutRes() {
        return R.layout.activity_content_aboard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Create!");

        netApiInterface = new MockApiClient();
        baseCategories =  netApiInterface.getBaseCategory();

        Log.d(TAG, baseCategories.toString());

        String pp = mPager==null ? "null" : "notnull";
        Log.d(TAG, "ViewPager: "+pp);

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),
                baseCategories);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener((ViewPager.OnPageChangeListener) mPagerAdapter);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter
                    implements ViewPager.OnPageChangeListener{
        List<BaseCategory> baseCategories;

        public ScreenSlidePagerAdapter(FragmentManager fm,
                                       List<BaseCategory> baseCategories) {
            super(fm);
            this.baseCategories = baseCategories;
            tvNumberPages.setText(""+1+"/"+getCount());
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "Position: "+position);
            ArrayList<String> urls = new ArrayList<>();
            int start = position*8;
            int end = start+8;
            if (baseCategories.size()<end) end=baseCategories.size();
            for (int i = position*8; i < end; i++) {
                String url = baseCategories.get(i).getUrl();
                urls.add(url);
            }
            return AboardMainFragment.getInstance(position*8, urls, getSupportFragmentManager());
            //return (Fragment) new AboardMainFragment();
        }

        @Override
        public int getCount() {
            return (baseCategories.size()+(8-1))/8;
        }


        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //tvNumberPages.setText(""+position+"/"+getCount());
        }

        @Override
        public void onPageSelected(int position) {
            tvNumberPages.setText(""+(position+1)+"/"+getCount());
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
