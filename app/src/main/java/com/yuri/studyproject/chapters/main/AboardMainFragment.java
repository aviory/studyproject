package com.yuri.studyproject.chapters.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;
import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseFragment;
import com.yuri.studyproject.chapters.category.CategoryActivity;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by Юрий on 13.10.2015.
 */
public class AboardMainFragment extends BaseFragment implements View.OnClickListener{
    private final static String TAG = AboardMainFragment.class.getSimpleName();
    private final static String NUMBER_START_CATEGORY = "start";
    private final static String LIST_BASE_CATEGORIES = "list";

    @Bind(R.id.glContainer)
    protected GridLayout glContainer;
    /*@Bind(R.id.imgButton)
    protected ImageButton imgButton;*/

    public static Fragment getInstance(int start, ArrayList<String> urlBaseCategory,
                                   FragmentManager manager){
        Fragment fragment = manager
                .findFragmentByTag(AboardMainFragment.class.getCanonicalName() + start);
        if (fragment==null) {
            fragment = new AboardMainFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(NUMBER_START_CATEGORY, start);
            bundle.putStringArrayList(LIST_BASE_CATEGORIES, urlBaseCategory);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_aboard_main;
    }

    @Override
    protected void afterCreateView() {
        Bundle args = getArguments();
        if (args==null) return;
        ArrayList<String> urls = args.getStringArrayList(LIST_BASE_CATEGORIES);
        if (urls==null) return;
        Log.d(TAG, "Size array: " + urls.size());
        int startPosition = args.getInt(NUMBER_START_CATEGORY);
        Log.d(TAG, "Start position: " + startPosition);

        //ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.imgButton);
        //GridLayout.LayoutParams params = imgButton.getLayoutParams();
        //glContainer.removeAllViews();

        /*LinearLayout llContent = (LinearLayout) v.findViewById(R.id.llContent);

        LinearLayout llHorisontal = getLayoutHorisontal();*/

        /*Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;
        int halfScreenWidth = (int)(screenWidth *0.5);
        int quarterScreenWidth = (int)(halfScreenWidth * 0.4);
        int quarterScreenHeight = (int) (screenHeight * 0.2);

        GridLayout.Spec row1 = GridLayout.spec(0, 2);
        GridLayout.Spec row2 = GridLayout.spec(2);
        GridLayout.Spec row3 = GridLayout.spec(3);
        GridLayout.Spec row4 = GridLayout.spec(4, 2);

        GridLayout.Spec col0 = GridLayout.spec(0);
        GridLayout.Spec col1 = GridLayout.spec(1);
        GridLayout.Spec colspan2 = GridLayout.spec(0, 2);
*/
        /*for (String url : urls) {
            ImageButton button = new ImageButton(getContext());


            GridLayout.LayoutParams first = new GridLayout.LayoutParams();
            first.width=halfScreenWidth;
            first.height=quarterScreenHeight;

            first.setGravity(Gravity.CENTER);

            //first.setGravity(GridLayout.LayoutParams.MATCH_PARENT);

            //button.setLayoutParams(first);

            glContainer.addView(button, first);
        }*/

        for (int i = 0; i <urls.size(); i++) {
            ImageButton imageButton = (ImageButton) glContainer.getChildAt(i);
            imageButton.setTag(i);
            imageButton.setOnClickListener(this);
            Picasso.with(getContext())
                    .load(urls.get(i))
                    .fit()
                    .into(imageButton);
        }
    }

    @Override
    public void onClick(View v) {
        CategoryActivity.launch(getActivity(), Integer.parseInt(v.getTag().toString()));
    }

   /* private LinearLayout getLayoutHorisontal() {
        LinearLayout llContainerHorisontal = new LinearLayout(getContext());
        ViewGroup.LayoutParams params = llContainerHorisontal.getLayoutParams();
        params.width= ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = 0;
        llContainerHorisontal.setOrientation(LinearLayout.HORIZONTAL);
        llContainerHorisontal.setWeightSum(0.25f);
        llContainerHorisontal.setLayoutParams(params);
        return llContainerHorisontal;
    }*/

}
