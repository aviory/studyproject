package com.yuri.studyproject.chapters.main;

import com.yuri.studyproject.chapters.base.BaseInterface.BaseApiOperation;
import com.yuri.studyproject.datastorage.provider.WebApiProvider;

/**
 * Created by Юрий on 24.10.2015.
 */
public class AppInit {
    public AppInit(){}

    public void init(){
        loadAppConfigFromSettingsProvider();
    }

    private void loadAppConfigFromSettingsProvider(){
        WebApiProvider webApiProvider = new WebApiProvider();
    }

    private void startApp(){}

    class GetAllCategories extends BaseApiOperation{

        GetAllCategories(Object request) {
            super(request);
        }

        @Override
        public void init() {

        }

        @Override
        public Object onResult(Object request) {
            return null;
        }

        @Override
        public Exception onError() {
            return null;
        }
    }
}
