package com.yuri.studyproject.datastorage.model;

import com.yuri.studyproject.domain.Country;
import com.yuri.studyproject.domain.Currency;
import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

/**
 * Created by Юрий on 24.10.2015.
 */
public class AppBaseEntities {
    private List<Category> categories;
    private List<Country> countries;
    private List<Currency> currencies;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }
}
