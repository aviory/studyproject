package com.yuri.studyproject.datastorage.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class AdvertiseContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.yuri.studyproject";

    private static final int ADVERTISE = 1;
    private static final int ADVERTISE_ID = 2;

    private DatabaseHelper databaseHelper;

    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, "advertise", ADVERTISE);
        sUriMatcher.addURI(AUTHORITY, "advertise/#", ADVERTISE_ID);
    }

    public AdvertiseContentProvider() {
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TableAdvertisOptions.TABLE_NAME);
        switch (sUriMatcher.match(uri)) {
            case ADVERTISE:
                break;
            case ADVERTISE_ID:
                String idItem = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(TableAdvertisOptions.TABLE_NAME + "=" + idItem);
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor c = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (sUriMatcher.match(uri) != ADVERTISE) {
            throw new IllegalArgumentException("URI не определен!" + uri);
        }
        ContentValues newValues = null;
        if (values != null) {
            newValues = new ContentValues(values);
        } else {
            values = new ContentValues();
        }
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        long rowId = db.insert(TableAdvertisOptions.TABLE_NAME, TableAdvertisOptions.KEY_DESCRIPTION, newValues);
        if (rowId > 0) {
            uri = ContentUris.withAppendedId(TableAdvertisOptions.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        throw new UnsupportedOperationException("ошибка вставки строки " + uri);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count = -1;
        switch (sUriMatcher.match(uri)) {
            case ADVERTISE:
                count = db.update(TableAdvertisOptions.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ADVERTISE_ID:
                String id = uri.getPathSegments().get(1);
                String finalWhere = TableAdvertisOptions.KEY_ID + "=" + id;
                if (selection != null) {
                    finalWhere = finalWhere + " AND " + selection;
                }
                count = db.update(TableAdvertisOptions.TABLE_NAME, values, finalWhere, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("URI не определен! " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int count = -1;
        switch (sUriMatcher.match(uri)) {
            case ADVERTISE:
                count = db.delete(TableAdvertisOptions.TABLE_NAME, selection, selectionArgs);
                break;
            case ADVERTISE_ID:
                String id = uri.getPathSegments().get(1);
                String finalWhere = TableAdvertisOptions.KEY_ID + "=" + id;
                if (selection != null) {
                    finalWhere = finalWhere + " AND " + selection;
                }
                count = db.delete(TableAdvertisOptions.TABLE_NAME, finalWhere, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("URI не определен! " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }


    private static class DatabaseHelper extends SQLiteOpenHelper {
        public static final String DATABASE_TABLE_NAME = TableAdvertisOptions.TABLE_NAME;
        public static final String DATABASE_TABLE_PARENT = TableCategoryOptions.TABLE_NAME;

        public static final String KEY_ID = TableAdvertisOptions.KEY_ID;
        public static final String KEY_FK_PARENT = TableAdvertisOptions.KEY_FK_PARENT_ID;
        public static final String KEY_STATUS = TableAdvertisOptions.KEY_STATUS;
        public static final String KEY_TITLE = TableAdvertisOptions.KEY_TITLE;
        public static final String KEY_DESCRIPTION = TableAdvertisOptions.KEY_DESCRIPTION;
        public static final String KEY_PRICE = TableAdvertisOptions.KEY_PRICE;
        public static final String KEY_COLLEGE_ID = TableAdvertisOptions.KEY_COLLEGE;
        public static final String KEY_DATE = TableAdvertisOptions.KEY_DATE;
        public static final String KEY_LIST_IMAGE_ID_ = TableAdvertisOptions.KEY_LIST_IMG;


        private static final String DATABASE_CREATE_TABLE_ADVERTISE =
                "create table " + DATABASE_TABLE_NAME + " ("
                        + KEY_ID + " integer primary key autoincrement, "
                      //  + " foreign key (" + KEY_FK_PARENT + ") references " + DATABASE_TABLE_PARENT + "(" + KEY_ID + "),"
                        + KEY_STATUS + " integer , "
                        + KEY_TITLE + " text , "
                        + KEY_DESCRIPTION + " text , "
                        + KEY_PRICE + " integer , "
                        + KEY_COLLEGE_ID + " integer , "
                        + KEY_DATE + " integer, "
                        + KEY_LIST_IMAGE_ID_ + " integer);";

        private Context ctx;

        DatabaseHelper(Context context) {
            super(context, TableAdvertisOptions.DB_NAME, null, TableAdvertisOptions.VERSION_DB);
            ctx = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE_TABLE_ADVERTISE);
            ContentValues cv = new ContentValues();
            cv.put(TableAdvertisOptions.KEY_TITLE,"title1");
            cv.put(TableAdvertisOptions.KEY_DESCRIPTION,"Desk1");
            cv.put(TableAdvertisOptions.KEY_PRICE,1000);
            db.insert(TableAdvertisOptions.TABLE_NAME,null,cv);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TableAdvertisOptions.TABLE_NAME);
            onCreate(db);
        }
    }
}
