package com.yuri.studyproject.datastorage.provider;

import com.yuri.studyproject.chapters.base.BaseInterface.BaseDataInterface;
import com.yuri.studyproject.chapters.base.BaseInterface.BaseDataSaveInterface;
import com.yuri.studyproject.domain.Ad;
import com.yuri.studyproject.domain.Country;
import com.yuri.studyproject.domain.Currency;
import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

/**
 * Created by Юрий on 24.10.2015.
 */
public class GenetalApiProvider implements BaseDataInterface, BaseDataSaveInterface {

    @Override
    public List<Category> getAllCategoties() {
        return null;
    }

    @Override
    public List<Country> getAllCountry() {
        return null;
    }

    @Override
    public List<Currency> getAllCurrency() {
        return null;
    }

    @Override
    public List<Category> getCategoriesFromParentId(long id) {
        return null;
    }

    @Override
    public List<Ad> getAdFromParentCategory(long id) {
        return null;
    }

    @Override
    public void storeAllCategories(List<Category> categories) {

    }

    @Override
    public void storeAllCountry(List<Country> countries) {

    }

    @Override
    public void storeAllCurrency(List<Currency> currencies) {

    }
}
