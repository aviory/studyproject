package com.yuri.studyproject.datastorage.provider;

import android.content.Context;
import android.content.SharedPreferences;

import com.yuri.studyproject.BuildConfig;

import java.util.Date;

/**
 * Created by Юрий on 20.10.2015.
 */
public class SettingsProvider {
    private Context context;
    private SharedPreferences sharedPreferences;

    private final static String file = "my_shared_pereferences";
    private static final String FIRST_START = "first_start";
    private final static String DATE_BASE_LOADED = "date_load_base_in_api";

    SettingsProvider(Context context) {
        this.context = context;
        sharedPreferences = context
                .getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    public boolean isFirstStart() {
        if (sharedPreferences.getBoolean(FIRST_START, true)) {
            sharedPreferences.edit().putBoolean(FIRST_START, false).commit();
            return true;
        } else return false;
    }

    public void setDateLoading(Date date) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DATE_BASE_LOADED, date.toString());
        editor.commit();
    }

    public Date getDateLoading() {
        sharedPreferences.getString(DATE_BASE_LOADED, "");
        return null;
    }

    public void setCustomData(String tag, String data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(tag, data);
        editor.commit();
    }

    public String getCustomData(String tag) {
        return sharedPreferences.getString(tag, null);
    }
}
