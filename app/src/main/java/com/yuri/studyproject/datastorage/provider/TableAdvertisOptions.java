package com.yuri.studyproject.datastorage.provider;

import android.net.Uri;

/**
 * Created by Jack_killer on 28.10.2015.
 */
public class TableAdvertisOptions {
    public static final String DB_NAME = "app_db";
    public static final int VERSION_DB = 3;
    public static final String TABLE_NAME = "advertise";
    public static final Uri CONTENT_URI = Uri.parse(
            "content://" + AdvertiseContentProvider.AUTHORITY + "/" + TABLE_NAME);

    public static final String KEY_ID = "_id";
    public static final String KEY_FK_PARENT_ID = "fk_parent_id";
    public static final String KEY_STATUS = "status";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_PRICE = "price";
    public static final String KEY_COLLEGE = "college";
    public static final String KEY_DATE = "date";
    public static final String KEY_LIST_IMG = "list_img";

}
