package com.yuri.studyproject.domain;

import android.database.Cursor;

import com.yuri.studyproject.datastorage.provider.TableAdvertisOptions;

import java.util.Date;
import java.util.List;

/**
 * Created by RST0 on 14.10.2015.
 */
public class Ad {
    public final static byte MSG_ACTIVE = 0;
    public final static byte MSG_INACTIVE = 1;
    public final static byte MSG_ARCHIVE = 2;

    private long id;
    private long parent_id;
    private byte status; //0:Active, 1:Inactive, 2:Archive
    private String title; //Short description of the item.
    private String description; // Detailed description of the item.
    private int price; //Price of the item in fixed point format
    private long college; //college id
    private Date date; // Date Ad
    private List<String> image; // First reference is a preview. All other are detailed images.

    public Ad(Cursor cursor) {
        this.id = cursor.getLong(cursor.getColumnIndex(TableAdvertisOptions.KEY_ID));
       // this.parent_id = cursor.getLong(cursor.getColumnIndex(TableAdvertisOptions.KEY_FK_PARENT_ID));
        this.status = (byte)cursor.getInt(cursor.getColumnIndex(TableAdvertisOptions.KEY_STATUS));
        this.title = cursor.getString(cursor.getColumnIndex(TableAdvertisOptions.KEY_TITLE));
        this.description = cursor.getString(cursor.getColumnIndex(TableAdvertisOptions.KEY_DESCRIPTION));
        this.price = cursor.getInt(cursor.getColumnIndex(TableAdvertisOptions.KEY_PRICE));
        this.college = cursor.getLong(cursor.getColumnIndex(TableAdvertisOptions.KEY_COLLEGE));
        this.date = new Date(cursor.getLong(cursor.getColumnIndex(TableAdvertisOptions.KEY_TITLE)));
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCollege(long college) {
        this.college = college;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public long getId() {

        return id;
    }

    public byte getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public long getCollege() {
        return college;
    }

    public Date getDate() {
        return date;
    }

    public List<String> getImage() {
        return image;
    }


    public long getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }
}
