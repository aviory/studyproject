package com.yuri.studyproject.domain;

/**
 * Created by RST0 on 14.10.2015.
 */
public class Category{
    private int id; //Category ID
    private int parentId; //ID of parent category
    private String title; //Title of category
    private String iconURL; //URL of category icon
}
