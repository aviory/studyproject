package com.yuri.studyproject.network;

import com.yuri.studyproject.network.pojo.BaseCategory;
import com.yuri.studyproject.network.pojo.Category;

import java.sql.Date;
import java.util.List;

/**
 * Created by Юрий on 13.10.2015.
 */
public interface NetApiInterface {
    public boolean isCategoryChanged(Date date);
    public List<BaseCategory> getBaseCategory();
    public List<Category> getCategories();
    public List<Category> getCategories(int parentId);
    //Account
    //AbstractAccountAuthenticator
}
