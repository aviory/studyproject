package com.yuri.studyproject.network.mockapi;

import com.yuri.studyproject.network.NetApiInterface;
import com.yuri.studyproject.network.pojo.BaseCategory;
import com.yuri.studyproject.network.pojo.Category;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Юрий on 13.10.2015.
 */
public class MockApiClient implements NetApiInterface {
    private String urlCatFirst =
            "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTUDQgbT9PFaERFaLbqP8" +
                    "sFsyq2r3sSYu6BtCj63z90tLEpALgo";
    private String urlCatSecond =
            "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTEb0rGk4uod5DQuMu0Cj" +
                    "lrjUqnp-LlGIRE1TVQu8HlnVWDyQtzbA";

    @Override
    public boolean isCategoryChanged(Date date) {
        return false;
    }

    @Override
    public List<BaseCategory> getBaseCategory() {
        ArrayList<BaseCategory> baseCategories = new ArrayList<>();
        for (int i = 0; i < 17; i++) {
            BaseCategory baseCategory = new BaseCategory();
            baseCategory.setCategory_id(i);
            baseCategory.setUrl(((i & 1) == 0 ? urlCatFirst : urlCatSecond));
            baseCategories.add(baseCategory);
        }
        return baseCategories;
    }

    @Override
    public List<Category> getCategories() {
        return null;
    }

    @Override
    public List<Category> getCategories(int parentId) {
        ArrayList<Category> categories = new ArrayList<>();
        Random r = new Random();
        int count = r.nextInt(20);
        for (int i = 0; i < count; i++) {
            Category category = new Category();
            category.setCityId(r.nextInt(100));
            category.setCategory_id(i);
            category.setItemName("Mockup category " + i);
            category.setParent_id(parentId);
            categories.add(category);
        }
        return categories;
    }
}
