package com.yuri.studyproject.network.netapi;

import com.yuri.studyproject.network.NetApiInterface;
import com.yuri.studyproject.network.pojo.BaseCategory;
import com.yuri.studyproject.network.pojo.Category;

import java.sql.Date;
import java.util.List;

/**
 * Created by Юрий on 13.10.2015.
 */
public class NetApiClient implements NetApiInterface {
    @Override
    public boolean isCategoryChanged(Date date) {
        return false;
    }

    @Override
    public List<BaseCategory> getBaseCategory() {
        return null;
    }

    @Override
    public List<Category> getCategories() {
        return null;
    }

    @Override
    public List<Category> getCategories(int parentId) {
        return null;
    }

}
