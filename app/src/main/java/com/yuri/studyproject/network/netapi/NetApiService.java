package com.yuri.studyproject.network.netapi;

import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Юрий on 24.10.2015.
 */
public interface NetApiService {

    @GET("/category/")
    public void getListCategoryFromParentId(@Path("id") String id, Callback<List<Category>> listCallback);



}
