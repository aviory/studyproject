package com.yuri.studyproject.network.netapi;

import com.yuri.studyproject.network.pojo.Category;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Path;

/**
 * Created by Юрий on 24.10.2015.
 */
public class RetrofitService implements NetApiService{

    RetrofitService(){

    }

    @Override
    public void getListCategoryFromParentId(@Path("id") String id, Callback<List<Category>> listCallback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://blabla.com")
                .build();

        NetApiService service = retrofit.create(NetApiService.class);
        service.getListCategoryFromParentId("0", new Callback<List<Category>>() {
            @Override
            public void onResponse(Response<List<Category>> response, Retrofit retrofit) {
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }
}
