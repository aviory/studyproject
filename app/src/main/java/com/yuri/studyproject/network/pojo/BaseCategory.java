package com.yuri.studyproject.network.pojo;

import java.io.Serializable;

/**
 * Created by Юрий on 13.10.2015.
 */
public class BaseCategory implements Serializable{
    private String url;
    private int category_id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "BaseCategory{" +
                "url='" + url + '\'' +
                ", category_id=" + category_id +
                '}'+"\n";
    }
}
