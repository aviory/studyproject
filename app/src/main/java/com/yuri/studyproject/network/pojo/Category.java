package com.yuri.studyproject.network.pojo;

/**
 * Created by Юрий on 13.10.2015.
 */
public class Category extends BaseCategory {
    private int parent_id;
    private String itemName;
    private int cityId;

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "Category{" +
                "parent_id=" + parent_id +
                ", itemName='" + itemName + '\'' +
                ", cityId=" + cityId +
                '}';
    }
}
