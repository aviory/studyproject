package com.yuri.studyproject.network.pojo;

/**
 * Created by Юрий on 13.10.2015.
 */
public class City {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
