package com.yuri.studyproject.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yuri.studyproject.R;
import com.yuri.studyproject.chapters.base.BaseActivity;
import com.yuri.studyproject.chapters.category.CategoryFragment;
import com.yuri.studyproject.domain.Ad;


public class AdvertiseAdapter extends CursorRecyclerViewAdapter<AdvertiseAdapter.ViewHolder>
        implements View.OnClickListener {

    private final static String TAG = AdvertiseAdapter.class.getSimpleName();
    private Context context;

    public AdvertiseAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_from_list_ad, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        itemView.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Ad ad = new Ad(cursor);
        viewHolder.mTextViewPrice.setText(ad.getPrice() + "USD");
        viewHolder.mTextViewTitle.setText(ad.getTitle());
        viewHolder.mTextViewCity.setText(ad.getTitle());
        viewHolder.mTextViewPath.setText(ad.getParent_id() + "");
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView mTextViewTitle;
        private TextView mTextViewPath;
        private TextView mTextViewCity;
        private TextView mTextViewPrice;

        public ViewHolder(View v) {
            super(v);
            mTextViewTitle = (TextView) v.findViewById(R.id.title_item);
            mTextViewPath = (TextView) v.findViewById(R.id.path_caregory);
            mTextViewCity = (TextView) v.findViewById(R.id.city_name);
            mTextViewPrice = (TextView) v.findViewById(R.id.price);
        }
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "OnClick: " + v.getTag());
        //CategoryActivity.launch(context, Integer.parseInt(v.getTag().toString()));
        CategoryFragment.launch((BaseActivity)context, v.getId());
    }
}