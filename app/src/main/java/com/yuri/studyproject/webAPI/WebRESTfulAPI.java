package com.yuri.studyproject.webAPI;

/**
 * Created by RST0 on 16.10.2015.
 */
public class WebRESTfulAPI {
    /**
     *  URI scheme construction:
     *      {POST/GET/PUT/DELETE} {URI}
     *
     *  Create entity
     *      POST {URL}/{Entity collection}/{id}
     *
     *
     *  Retrieve entity by ID
     *      GET {URL}/{Entity collection}/{id}
     *
     *
     *  Retrieve entity collection
     *      GET {URL}/{Entity collection}
     *
     *
     *  Update entity by ID
     *      PUT {URL}/{Entity collection}/{id}
     *
     *
     *  Delete entity by ID
     *      DELETE {URL}/{Entity collection}/{id}
     *
     *
     *  Delete entity collection
     *      DELETE {URL}/{Entity collection}
     *
     *
     *  Operations allowed to be performed by a mobile user:
     *
     *
     *  Entity "Ad"
     *
     *      - create new "Ad";
     *      - retrieve all "Ad" objects;
     *      - retrieve "Ad" object by ID;
     *      - find "Ad" objects by wildcard;
     *      - update an existing "Ad" object;
     *      - delete "Ad" object from server;
     *      - delete list of "Ad" objects;
     *
     *
     * Entity "Category"
     *
     *      - retrieve all "Category" objects;
     *      - retrieve "Category" object by ID;
     *
     *
     * Entity "College"
     *
     *      - retrieve all "College" objects;
     *      - retrieve "College" object by ID;
     *
     *
     * Entity "Contact"
     *
     *      - retrieve all "Contact" objects;
     *      - retrieve "Contact" object by ID;
     *
     *
     * Entity "Country"
     *
     *      - retrieve all "Country" objects;
     *      - retrieve "Country" object by ID;
     *
     *
     * Entity "Currency"
     *
     *      - retrieve all "Currency" objects;
     *      - retrieve "Currency" object by ID;
     *
     *
     * Entity "Dialog"
     *
     *      - create new "Dialog" object;
     *      - retrieve all "Dialog" objects;
     *      - retrieve "Dialog" object by ID;
     *      - find "Dialog" objects by wildcard;
     *
     *
     *  Entity "Message"
     *
     *      - create new "Message";
     *      - retrieve all "Message" objects;
     *      - retrieve "Message" object by ID;
     *      - find "Message" objects by wildcard;
     *      - update an existing "Message" object;
     *
     *
     *  Entity "User"
     *
     *      - create new user account;
     *      - send credentials / retrieve authentication token;
     *      - update data in user account;
     *      - delete user account.
     *
     *
     * Operation examples
     *      Retrieve all category objects.
     *
     * Request example
     *      GET http://www.studyproject.com/categories
     *
     * Respond example
     *      {
     *          "object":"JSON.List.Category",
     *          "value":[
     *              {
     *                  "id":"1",
     *                  "parentId":"0",
     *                  "title":"Furniture",
     *                  "iconURL":"http://www.studyproject.com/categories/1/images/cat1.png",
     *              },
     *              {
     *                  "id":"2",
     *                  "parentId":"0",
     *                  "title":"Equipment",
     *                  "iconURL":"http://www.studyproject.com/categories/2/images/cat2.png",
     *              }
     *          ]
     *      }
     *
     *
     */

}
